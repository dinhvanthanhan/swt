function checkDate() {
    var day = +document.getElementById("day1").value;
    var month = +document.getElementById("month1").value;
    var year = +document.getElementById("year1").value;

    if (day > 0 && day < 32) {
        if (month > 0 && month < 13) {
            switch (month) {
                case 2: {
                    if (day > 29) {
                        document.getElementById("notification1").innerHTML = `February doesn't have this date`
                    }
                    else if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) && day < 30) {
                        document.getElementById("notification1").innerHTML = `Success`
                    }
                    else if (day == 29) {
                        document.getElementById("notification1").innerHTML = `Fail because ` + year + ` not a leap year`
                    }
                    else {
                        document.getElementById("notification1").innerHTML = `Success`
                    }
                }

                case 4:
                case 6:
                case 9:
                case 11: {
                    if (day == 31) {
                        document.getElementById("notification1").innerHTML = `Fail because this month does not have day 31`
                    } else {
                        document.getElementById("notification1").innerHTML = `Success`
                    }
                }

                default: {
                    document.getElementById("notification1").innerHTML = `Success`
                }


            }
        } else {
            document.getElementById("notification1").innerHTML = `Fail because month smaller than 0 or month bigger than 12`
        }
    } else {
        document.getElementById("notification1").innerHTML = `Fail because day smaller than 0 or day bigger than 31`
    }
}

function checkMonth() {
    var month = +document.getElementById("month2").value;
    var year = +document.getElementById("year2").value;

    if (month > 0 && month < 13) {
        switch (month) {
            case 2: {
                if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))) {
                    document.getElementById("notification2").innerHTML = `February in year ` + year + ` has 29 day`
                } else {
                    document.getElementById("notification2").innerHTML = `February in year ` + year + ` has 28 day`
                }
            }

            case 4:
            case 6:
            case 9:
            case 11:
                document.getElementById("notification2").innerHTML = `Month ` + month + ` year ` + year + ` has 30 day`

            default: {
                document.getElementById("notification2").innerHTML = `Month ` + month + ` year ` + year + ` has 31 day`
            }
        }
    }
}